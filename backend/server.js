const express = require('express');
// const request = require('request');
const axios = require('axios');

const app = express()
const port = 5000;

app.get('/getheadlines', async(req, res) => {
    try {
        const response = await axios.get(`https://newsapi.org/v2/top-headlines?country=in&apiKey=de0f457550d04a999b6d802c86b3bd9a`);
        res.json(response.data);
    } catch (error) {
        console.error('Error fetching news:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
})


app.listen(port, () => console.log(`App is listening on port ${port}`));