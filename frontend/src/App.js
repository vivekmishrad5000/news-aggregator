import "./App.css";
import News from "./components/News";
import { Routes, Route, BrowserRouter, Navigate} from "react-router-dom";
import Category from "./components/Category";
import EverythingNews from "./components/EverythingNews";
import Header from "./components/Header";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
        <Route path="/" element={<News />} />
        <Route path="/category/:category" element={<Category />} />
        <Route path="*" element={<Navigate to="/category/general" />} />
        <Route path="/category/world-news" element={<EverythingNews />} /> 
        </Routes>
        </BrowserRouter>
    </>
  );
}

export default App;
