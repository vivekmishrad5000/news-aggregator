import React from 'react'
import { useEffect, useState } from 'react'
import {Box, Heading, SimpleGrid} from '@chakra-ui/react'
import axios from 'axios'
import NewsItem from './NewsItem'
import Header from './Header'

const News = () => {
    const [newss, setNews] = useState([])
    const [query, setQuery] = useState('')
    const [filteredNews, setFilteredNews] = useState([])

    useEffect(() => {
        const pageSize = 38;
        axios.get('/getheadlines')
        .then(response => {
            setNews(response.data.articles);
            setFilteredNews(response.data.articles);
        })
    },[])

    useEffect(() => {
      setFilteredNews(
        newss.filter(news =>
          news.title.toLowerCase().includes(query.toLowerCase())
        )
      );
    },[query, newss])

  return (
    <>
    <Header query = {query} setQuery={setQuery} />
    <Box mt={4}>
        <Heading as='h2' backgroundColor='#fefefe' color='#222225' textAlign='center' textTransform='uppercase'>Top Headlines</Heading>
        <SimpleGrid spacing={8} templateColumns='1fr 1fr 1fr' backgroundColor='#fefefe'>

        {filteredNews.map((news) => (
           <NewsItem title = {news.title} imageUrl = {news.urlToImage} newsUrl = {news.url} key={news.url} />
        ))}
        </SimpleGrid>
    </Box>

    </>
  )
}

export default News