import {Box,Heading,SimpleGrid,Link,Card,CardFooter,Button,CardBody,Image, Text} from '@chakra-ui/react'
import React, { useEffect, useState } from 'react'
import image from './images/backupnews.png'
import axios from 'axios'
import Header from './Header'
import { useParams } from 'react-router-dom'

const Category = () => {
    const [sports, setSports] = useState([])
    const {category} = useParams()
    // eslint-disable-next-line
    const [query, setQuery] = useState('')
    const [filteredNews, setFilteredNews] = useState([])

    useEffect(() => {
        const pageSize = 65;
        axios.get(`https://newsapi.org/v2/top-headlines?country=in&category=${category}&apiKey=de0f457550d04a999b6d802c86b3bd9a&pageSize=${pageSize}`)
        .then(response => {
            setSports(response.data.articles)
            setFilteredNews(response.data.articles)
        })
    },[category])

    useEffect(() => {
      setFilteredNews(
        sports.filter(sport =>
          sport.title.toLowerCase().includes(query.toLowerCase())
        )
      )
    },[query, sports])

  return (
    <>
    <Header query={query} />
    <Box mt={4}>
    <Heading as='h2' backgroundColor='#fefefe' color='#222225' textAlign='center' textTransform='uppercase'>{category}</Heading>
    <SimpleGrid spacing={8} templateColumns='1fr 1fr 1fr' backgroundColor='#fefefe'>

    {filteredNews.map((sport) => (
    <Card backgroundColor='#f2f2f2' color='black' alignItems='center' fontSize='lg' width='95%' margin='0 auto 0 auto' mt={4} boxShadow='md'>
    <Image src={!sport.urlToImage? image : sport.urlToImage } height='18rem'  alt='news' mt='0.2rem'/>
    <CardBody>
      <Text>{sport.title}</Text>
    </CardBody>
    <CardFooter>
    <Link href={sport.url} target='_blank'>
      <Button background='#222225' color='#fefefe' _hover={{ bg:'#222225', opacity:0.8}}>Read More</Button>
      </Link>
    </CardFooter>
  </Card>
  ))}
  </SimpleGrid>
  </Box>
  </>
  )
}

export default Category