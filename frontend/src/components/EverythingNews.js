import React from 'react'
import {Heading, SimpleGrid, Text, Image, Link, Box, Card, CardBody, CardFooter, Button} from '@chakra-ui/react'
import { useState, useEffect } from 'react'
import axios from 'axios';
import Header from './Header';
import image from './images/backupnews.png'


const EverythingNews = () => {
    const [everyNews, setEveryNews] = useState([])
    const [filteredEveryNews, setFilteredEveryNews] = useState([]);
    const [query, setQuery] = useState('')

    useEffect(() => {
        const pageSize = 100;
        axios.get(`https://newsapi.org/v2/everything?q=general&language=en&apiKey=de0f457550d04a999b6d802c86b3bd9a&pageSize=${pageSize}`)
        .then(response => {
            setEveryNews(response.data.articles)
            setFilteredEveryNews(response.data.articles)
        })
    },[])

    useEffect(() => {
        setFilteredEveryNews(
            everyNews.filter(news =>
                news.title.toLowerCase().includes(query.toLowerCase())
            )
        )
    },[query, everyNews])

  return (
    <>
    <Header query={query} setQuery={setQuery} />
    <Box mt={4}>
    <Heading as='h2' backgroundColor='#fefefe' color='#222225' textAlign='center'>World News</Heading>
    <SimpleGrid spacing={8} templateColumns='1fr 1fr 1fr' backgroundColor='#fefefe'>
        {filteredEveryNews.map((news) => (
            <Card backgroundColor='#f2f2f2' color='black' alignItems='center' fontSize='lg' width='95%' margin='0 auto 0 auto' mt={4} boxShadow='md'>
            <Image src={!news.urlToImage? image : news.urlToImage } height='18rem'  alt='news' mt='0.2rem'/>
            <CardBody>
      <Text>{news.title}</Text>
    </CardBody>
    <CardFooter>
    <Link href={news.url} target='_blank'>
      <Button background='#222225' color='#fefefe' _hover={{ bg:'#222225', opacity:0.8}}>Read More</Button>
      </Link>
    </CardFooter>
            </Card>
        ))}
    </SimpleGrid>
    </Box>
    </>
  )
}

export default EverythingNews