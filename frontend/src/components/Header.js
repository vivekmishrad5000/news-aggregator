import {
  Flex,
  Heading,
  Input,
  Link,
  InputGroup,
  InputLeftElement,
  Icon,
  MenuButton,
  Button,
  MenuList,
  MenuItem,
  Menu,
} from "@chakra-ui/react";
import React from "react";
import { TiHome } from "react-icons/ti";
import {IoChevronDown} from 'react-icons/io5'
import {HiSearch} from 'react-icons/hi'
import { Link as RouterLink } from "react-router-dom";

const Header = ({query, setQuery}) => {

  return (
    <>
    <Flex
      as="nav"
      justifyContent="space-between"
      alignItems="center"
      color="white"
      background="#222225"
      w="100%"
      px="6"
      py="4"
    >
      <Heading as="h4">News Express</Heading>
      <Flex background="white" borderRadius="lg">
        <InputGroup display={{ base: 'none', md: 'flex' }} maxW="sm" bgColor={'#fefefe'} borderRadius={'5'}>
                <InputLeftElement
                    pointerEvents="auto"
                    children={<Icon as={HiSearch} color="gray.300" />}
                />
                <Input type="text" placeholder="Search News" color='black' value={query} onChange={(e) => setQuery(e.target.value)}/>
            </InputGroup>
      </Flex>
      <Flex gap={4} fontSize="xl" alignItems='center'>
        <Link _hover={{ textDecor: "none", opacity: "0.7" }} as={RouterLink} to='/'>
          <TiHome size='35px' />
        </Link>
        <Menu>
            <MenuButton
              as={Button}
              rightIcon={<IoChevronDown />}
              _hover={{ textDecor: "none", opacity: "0.0.7" }}
            >
              Category
            </MenuButton>
            <MenuList color='black'>
            <MenuItem as={RouterLink} to="/category/world-news" >
                World News
              </MenuItem>
              <MenuItem as={RouterLink} to="/category/technology" >
                Technology
              </MenuItem>
              <MenuItem as={RouterLink} to="/category/sports" >
                Sports
              </MenuItem>
              <MenuItem as={RouterLink} to="/category/business" >
                Business
              </MenuItem>
              <MenuItem as={RouterLink} to="/category/health" >
                Health
              </MenuItem>
              <MenuItem as={RouterLink} to="/category/entertainment" >
                Entertainment
              </MenuItem>
            </MenuList>
          </Menu>
      
      </Flex>
    </Flex>

    </>
  );
};

export default Header;
