import React from 'react'
import {Link,Card,CardFooter,Button,CardBody,Image, Text} from '@chakra-ui/react'
import image from './images/backupnews.png'

const NewsItem = ({title, newsUrl, imageUrl}) => {
  return (
    <Card backgroundColor='#f2f2f2' color='black' alignItems='center' fontSize='lg' width='95%' margin='0 auto 0 auto' mt={4} boxShadow='md' borderRadius='0.5rem'>
    <Image src={!imageUrl? image : imageUrl  } height='18rem'  alt='news' mt='0.3rem' borderRadius='0.5rem'/>
    <CardBody>
      <Text>{title}</Text>
    </CardBody>
    <CardFooter>
    <Link href={newsUrl} target='_blank'>
      <Button background='#222225' color='#fefefe' _hover={{ bg:'#222225', opacity:0.8}}>Read More</Button>
      </Link>
    </CardFooter>
  </Card>
  )
}

export default NewsItem